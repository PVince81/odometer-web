import Axios from "axios";

/**
 * Update session record based on history
 */
function computeSessionRecord(session) {
	if (!session.history || !session.history.length) {
		return;
	}

	let lastRecord = session.history[session.history.length - 1];
	session.record.distance = lastRecord.distance;

	let averageSpeed = 0.0;

	session.history.forEach((entry) => {
		averageSpeed += entry.speed;
	});

	session.record.average_speed = averageSpeed / session.history.length;

	return session;
}

function updateSession(sessions, updatedSession) {
	let sessionIndex = sessions.findIndex((element) => {
		return element.session_id === updatedSession.session_id;
	});

	if (sessionIndex === -1) {
		// insert at the top
		sessions.unshift(updatedSession);
	} else {
		// update in list
		sessions.splice(sessionIndex, 1, updatedSession); 
	}
}

export default function createSessionStore(api) {
	return {
		state: {
			sessions: {
				loading: false,
				error: null,
				records: null
			},
			session: {
				loading: false,
				error: null,
				record: null,
				history: null
			}
		},

		mutations: {
			// sessions
			LOADING_SESSIONS(state) {
				state.sessions.loading = true;
				state.sessions.records = null;
			},

			SET_SESSIONS(state, data) {
				state.sessions.records = data;
			},

			FINISH_SESSIONS(state) {
				state.sessions.loading = false;
				state.sessions.error = null;
			},

			FAILED_SESSIONS(state, error) {
				console.error(error);
				state.sessions.loading = false;
				state.sessions.error = error;
				state.sessions.records = null;
			},

			SOCKET_SESSION_START(state, data) {
				if (state.sessions.records === null) {
					return;
				}

				console.log('session_start', data);
				updateSession(state.sessions.records, data);
			},
			SOCKET_SESSION_END(state, data) {
				if (state.sessions.records === null) {
					return;
				}

				console.log('session_end', data);
				updateSession(state.sessions.records, data);
			},

			LOADING_SESSION(state) {
				state.session.loading = true;
				state.session.record = null;
			},

			SET_SESSION(state, data) {
				state.session.record = data.sessionData;
				state.session.history = data.historyData;
			},

			FINISH_SESSION(state) {
				state.session.loading = false;
				state.session.error = null;
			},

			FAILED_SESSION(state, error) {
				state.session.loading = false;
				state.session.error = error;
				state.session.record = null;
			},

			FAILED_SESSION_ERROR(state, error) {
				state.session.loading = false;
				state.session.error = error;
			},

			UPDATING_SESSION(state, session) {
				session.loading = true;
			},

			DELETING_SESSION(state, session) {
				session.loading = true;
			},

			DELETED_SESSION(state, session) {
				const sessionIndex = state.sessions.records.indexOf(session);
				if (sessionIndex < 0) {
					return;
				}
				state.sessions.records.splice(sessionIndex, 1);
				if (state.session && state.session.session_id === session.session_id) {
					state.session = null;
				}
			},

			SOCKET_HISTORY(state, data) {
				console.log('history', data);
				if (!data) {
					return;
				}
				if (state.session && state.session.record && state.session.record.session_id === data.session_id) {
					state.session.history.push(data);
					computeSessionRecord(state.session);
				}
			},
		},

		actions: {
			FETCH_SESSIONS(context, deviceId) {
				context.commit('LOADING_SESSIONS');

				let url = api + '/api/v1.0/sessions';

				if (deviceId) {
					url = api + `/api/v1.0/devices/${deviceId}/sessions`;
				}

				Axios.get(url)
					.then((response) => {
						context.commit("SET_SESSIONS", response.data);
						context.commit("FINISH_SESSIONS");
					})
					.catch((error) => {
						context.commit("FAILED_SESSIONS", error);
					});
			},

			FETCH_SESSION(context, sessionId) {
				context.commit('LOADING_SESSION');

				Axios.all([
					Axios.get(api + '/api/v1.0/sessions/' + sessionId),
					Axios.get(api + '/api/v1.0/sessions/' + sessionId + '/history')
				]).then(Axios.spread((sessionResponse, historyResponse) => {
					context.commit("SET_SESSION", {sessionData: sessionResponse.data, historyData: historyResponse.data});
					context.commit("FINISH_SESSION");
				}))
				.catch(Axios.spread((sessionError, historyError) => {
					context.commit("FAILED_SESSION", sessionError, historyError);
				}));
			},

			UPDATE_SESSION(context, startTime) {
				let session = context.state.session.record;
				console.log('Updating session', session, startTime);
				context.commit('UPDATING_SESSION', session);
				Axios.patch(
					api + '/api/v1.0/sessions/' + session.session_id,
					{
						'start_time': startTime
					}
				).then((response) => {
					if (response.status === 200) {
						console.log('Session updated, refetching...');
						context.dispatch('FETCH_SESSION', session.session_id);
					}
				})
				.catch((error) => {
					let response = error.response;
					let message = 'Error updating session, code ' + response.status;
					console.error('Failed to update session', response);
					if (response.data.error) {
						message = response.data.text;
					}
					context.commit("FAILED_SESSION_ERROR", message);
				});
			},

			DELETE_SESSION(context, session) {
				context.commit('DELETING_SESSION', session);
				Axios.delete(
					api + '/api/v1.0/sessions/' + session.session_id
				).then((response) => {
					if (response.status === 204) {
						context.commit('DELETED_SESSION', session);
					}
					// TODO: error handling
				});
			}
		}
	};
}

