/* global __dirname, require, module, process */
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const WriteFilePlugin = require('write-file-webpack-plugin');

const distDir = __dirname + '/dist';

var debugMode = true;

if (process.env.NODE_ENV === 'production') {
	debugMode = false;
}

module.exports = {
	mode: 'development',
	devtool: 'cheap-eval-source-map',
	entry: __dirname + '/src/app.js',
	output: {
		path: distDir,
		filename: 'bundle.js'
	},
	devServer: {
		inline: false,
		contentBase: distDir,
		port: 3000,
		hot: false,
	},
	module: {
		rules: [{
				test: require.resolve('uikit'),
				loader: 'expose-loader?UIkit'
			}, {
				test: /\.js$/,
				loader: 'babel-loader',
				include: [__dirname + '/src']
			}, {
				test: /\.scss?$/,
				loader: 'style-loader!css-loader!sass-loader'
			}, {
				test: /\.vue$/,
				loader: 'vue-loader',
				options: {
					loaders: {
						scss: 'vue-style-loader!css-loader!sass-loader',
						less: 'vue-style-loader!css-loader!less-loader'
					}
				}
			}
		]
	},
	plugins: [
		new WriteFilePlugin(),
		new CopyWebpackPlugin([{
			from: __dirname + '/static/',
			to: distDir + '/'
		}]),
		new CopyWebpackPlugin([{
			from: __dirname + '/node_modules/uikit/dist/css/uikit.min.css',
			to: distDir + '/css/'
		}]),
		new webpack.DefinePlugin({
			__DEBUG__: debugMode
		})
	]
};
