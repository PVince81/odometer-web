import Axios from "axios";

export default function createDevicesStore(api) {
	return {
		state: {
			loading: false,
			error: null,
			records: null
		},

		mutations: {
			// sessions
			LOADING_DEVICES: function(state) {
				state.loading = true;
				state.records = null;
			},

			SET_DEVICES: function(state, data) {
				state.records = data;
			},

			FINISH_DEVICES: function(state) {
				state.loading = false;
				state.error = null;
			},

			FAILED_DEVICES: function(state, error) {
				state.loading = false;
				state.error = error;
				state.records = null;
			}
		},

		actions: {
			FETCH_DEVICES: function(context) {
				context.commit('LOADING_DEVICES');

				Axios.get(api + '/api/v1.0/devices')
					.then((response) => {
						context.commit("SET_DEVICES", response.data);
						context.commit("FINISH_DEVICES");
					})
					.catch((error) => {
						context.commit("FAILED_DEVICES", error);
					});
			}
		}
	};
}

