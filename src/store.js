import Vue from 'vue';
import Vuex from 'vuex';

import createSessionsStore from './stores/sessions.js';
import createDevicesStore from './stores/devices.js';
import monitorStore from './stores/monitor.js';

Vue.use(Vuex);

export default function createStore(api) {
	return new Vuex.Store({
		modules: {
			monitor: monitorStore,
			devices: createDevicesStore(api),
			sessions: createSessionsStore(api)
		},

		state: {
			currentDevice: null,
		},

		computed: {
			sessions: function() {
				return this.state.sessions;
			},
			session: function() {
				return this.state.session;
			}
		}
	});
}

