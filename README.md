# Odometer Web

The Odometer Web front-end is a client for the [Odometer Server](https://gitlab.com/PVince81/odometer-server).

It is designed to visualize cycling sessions on a stationary bicycle in near real-time.

## Features

- Monitor live cycling data provided through [Socket.io](https://socket.io/) by an [Odometer Server](https://gitlab.com/PVince81/odometer-server) which 
- View past cycling sessions stored on an [Odometer Server](https://gitlab.com/PVince81/odometer-server) which 

## Requirements

- Node JS >= 6.9 for compiling the front-end code
- yarn >= v1.19.0
- Nginx or any web server for serving static web pages
- An instance of [Odometer Service](https://gitlab.com/PVince81/odometer)
- An instance of [Odometer Server](https://gitlab.com/PVince81/odometer-server) connected to the Odometer Service

Note: it is possible to run all the Odometer services together with Nginx on the same Raspberry Pi Zero.

## Installation

```
% yarn install
% yarn run build:production
```

Then copy the contents of the "dist" directory into your web server's content directory, like for example "/var/www/html/odometer".

## Running

Run the chosen web server and open the Odometer web front-end location in the web browser.

## Debugging

Run a local development server with `yarn run start:dev`.
The current version assumes that the Odometer Server is running on `http://localhost:5000` but can be adjusted in src/app.js

