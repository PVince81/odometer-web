export default {
	state: {
		deviceId: null,
		connectionStatus: 'disconnected',
		rotations: 0,
		elapsed: 0,
		speedRpm: 0.0,
		speedKmh: 0.0,
		distance: 0.0,
		heartRate: 0.0
	},

	computed: {
		selectedDeviceId() {
			return parseInt(this.$route.params.deviceId, 10);
		}
	},

	watch: {
		selectedDeviceId(newId, oldId) {
			if (oldId) {
				this.dispatch('MONITOR_STOP', oldId);
			}
			if (newId) {
				this.dispatch('MONITOR_START', newId);
			}
		}
	},

	mutations: {
		// Monitor
		SOCKET_CONNECT(state) {
			state.connectionStatus = 'connected';
			// FIXME: try with vuex-router-sync to get access to route values
			this.dispatch('MONITOR_START', 1);
		},
		SOCKET_DISCONNECT(state) {
			state.connectionStatus = 'disconnected';
		},
		SOCKET_CONNECTING(state) {
			state.connectionStatus = 'connecting';
		},
		SOCKET_CONNECT_ERROR(state) {
			state.connectionStatus = 'connect_error';
		},
		SOCKET_REQUEST_ERROR(state, data) {
			console.error(data);
		},
		SOCKET_MONITOR(state, data) {
			state.elapsed = data.elapsed;
			state.distance = data.distance / 100.0;
			state.heartRate = data.heartRate;
			state.speedRpm = data.speedRpm;
			state.speedKmh = data.speedKm;
			state.rotations = data.rotations;
		}
	},

	actions: {
		MONITOR_START: function(context, deviceId) {
			if (context.state.connectionStatus !== 'connected') {
				console.error('startMonitor: not connected');
				return;
			}
			console.log('MONITOR_START', deviceId);
			this._vm.$socket.emit('start_monitor_device', deviceId);
			context.state.deviceId = deviceId;
		},
		MONITOR_STOP: function(context, deviceId) {
			if (context.state.connectionStatus !== 'connected') {
				console.error('MONITOR_STOP: not connected');
				return;
			}
			console.log('MONITOR_STOP', deviceId);
			this._vm.$socket.emit('stop_monitor_device', deviceId);
			context.state.deviceId = null;
		}
	}
};
