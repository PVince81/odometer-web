import moment from 'moment';
import momentDurationFormatSetup from 'moment-duration-format';
momentDurationFormatSetup(moment);

const numberFormat = new Intl.NumberFormat('en-US', { maximumFractionDigits: 1, minimumFractionDigits: 1 });

export default {
	methods: {
		formatDuration(time) {
			return moment.duration(time, 'seconds').format('mm:ss');
		},

		formatFloat(number) {
			return numberFormat.format(number);
		},

		formatSpeedKmh(speedKmh) {
			return this.formatFloat(speedKmh) + ' km/h';
		},

		formatSpeedRpm(speedRpm) {
			return this.formatFloat(speedRpm) + ' RPM';
		},

		formatDistance(distanceM) {
			if (!distanceM) {
				return '0 m';
			}
			if (distanceM < 1000) {
				return this.formatFloat(distanceM) + ' m';
			} else {
				return this.formatFloat(distanceM / 1000.0) + ' km';
			}
		},

		formatDate(time) {
			return moment.unix(time).calendar();
		},

		formatDateTimeLong(time) {
			return moment.unix(time).format('LLLL');
		}
	}
}
