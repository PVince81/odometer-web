/* global __DEBUG__ */
import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';
import Vue from 'vue';
import VueRouter from 'vue-router';
import vueHeadful from 'vue-headful';

import App from './App.vue';

import createStore from './store.js';

import socketio from 'socket.io-client';
import VueSocketIO from 'vue-socket.io';

// Mixins
import formats from './formats.js';

Vue.mixin(formats);

// Components
import Monitor from './components/Monitor.vue';
import Sessions from './components/Sessions.vue';
import Session from './components/Session.vue';
import DeviceSelector from './components/DeviceSelector.vue';

Vue.use(VueRouter);

Vue.component('vue-headful', vueHeadful);

import moment from 'moment';
moment.locale('en', {
    calendar : {
        sameElse : 'll LT'
    }
});

const routes = [
	{ path: '/devices/', alias: '/', component: DeviceSelector },
	{ path: '/devices/:deviceId/monitor', alias: '/devices/:deviceId/', component: Monitor },
	{ path: '/devices/:deviceId/sessions', component: Sessions },
	{ path: '/devices/:deviceId/sessions/:id', component: Session }
];
const router = new VueRouter({
	routes,
	linkActiveClass: 'uk-active'
});


var debug = __DEBUG__;

import vueConfig from 'vue-config';
var configs = {};

if (debug) {
	console.log('Running in debug mode');
	configs.API = 'http://localhost:5000';
	configs.SOCKET_API = configs.API;
	configs.SOCKET_PATH = '/socket.io';
} else {
	// FIXME: might change depending on default route and install path
	configs.API = location.protocol + '//' + location.host + location.pathname;
	configs.SOCKET_API = location.protocol + '//' + location.host;
	configs.SOCKET_PATH = location.pathname + 'socket.io';
}

Vue.use(vueConfig, configs);

const store = createStore(configs.API);
export const SocketInstance = socketio(configs.SOCKET_API, {path: configs.SOCKET_PATH});

Vue.use(VueSocketIO, SocketInstance, store);

// loads the Icon plugin
UIkit.use(Icons);

new Vue({
	el: '#app',
	router,
	store: store,
    render: h => h(App)
});
